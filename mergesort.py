import random 
import unittest
from time import time
import matplotlib.pyplot as plt

class mergeSortTest(unittest.TestCase):
    def testSort(self):
        data=[11,15,14,12,1,3]
        mergeSort(data)
        self.assertListEqual(data,[1,3,11,12,14,15])

def mergeSort(array):
    if len(array)>1:
        mid = len(array)//2
        L = array[:mid]
        R = array[mid:]
        i = j = k = 0
        
        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                array[k] = L[i]
                i+=1
            else:
                array[k] = R[j]
                j+=1
            k+=1
        
        while i < len(L):
            array[k] = L[i]
            i+=1
            k+=1
            
        while j < len(R):
            array[k] = R[j] 
            j+=1
            k+=1
def plot():
    print("The calculations are running")
    executionTime_dic = {}
    for i in range (500,10000,1000):
        randomValue = random.sample(range(i),i)
        start=time()
        mergeSort(randomValue)
        end=time()
        calculation=end-start
        executionTime_dic[i] = calculation
    exeTime = list(executionTime_dic.values())
    inpSize = list(executionTime_dic.keys())
    plt.plot(inpSize, exeTime)
    plt.xlabel('Input Size(n)')
    plt.ylabel('Execution Time (sec)')
    plt.title("Mergesort")
    plt.xticks(inpSize)
    plt.yticks(exeTime)
    plt.show()

data = [11,15,14,12,1,3]
mergeSort(data)
print(data)
plot()


#To Test Merge Sort Uncomment Below Two code and Comment code outside function

# if __name__ == '__main__':
#    unittest.main()
    
    
            