import unittest
import random
from time import time
import matplotlib.pyplot as plt

class TestInsertionSort(unittest.TestCase):
    def testSort(self):
        array = [11,15,14,12,1,3]
        insertionSort(array)
        self.assertListEqual(array,[1,3,11,12,14,15])









def insertionSort(array):
    for i in range(1,len(array)):
        key = array[i]
        j = i-1
        while j>=0 and key < array[j]:
            array[j+1] =  array[j]
            j -=1
        array[j+1] = key


def plot():
    print("The calculations are running")
    executionTime_dic = {}
    for j in range (500,10000,1000):
        randomValue = random.sample(range(j),j)
        start=time()
        insertionSort(randomValue)
        end=time()
        calc=end-start
        executionTime_dic[j] = calc
    exeTime = list(executionTime_dic.values())
    inpSize = list(executionTime_dic.keys())
    plt.plot(inpSize, exeTime)
    plt.xlabel('Input Size(n)')
    plt.ylabel('Execution Time (sec)')
    plt.title("Insertion Sort")
    plt.xticks(inpSize)
    plt.yticks(exeTime)
    plt.show()

array = [11,15,14,12,1,3]
insertionSort(array)
plot()

        
        